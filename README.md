[![Codeship Status for Myl0g/humanGeography](https://app.codeship.com/projects/2adab610-4b29-0136-5648-12c81f08e2cf/status?branch=master)](https://app.codeship.com/projects/292875)

Contains different calculative methods for use by Human Geographers.

Among them are:
* Crude Birth Rate
* Crude Death Rate
* Rate of Natural Increase
* Doubling time
* Net Migration Rate
* Population growth (either as a decimal or a percent)
* Total Fertility Rate

For the full code documentation, click [here](https://myl0g.github.io/humanGeography/).
